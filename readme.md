# Music Organizer

I put together a few basic python utilities to help in handling the copy of my Google Play Music library, that I downloaded to my local PC.

The problem was that Google nested them `artist/album/song`, which is fine, as long as the albums are homogeneous. But as a classical music consumer, most of my albums consist of multiple composers (which are often substituted for the artist), and that led to a rather fractured state, where each album directory would contain only one or two songs, and there was no easy way to play the whole album.

`song_lister.py` will build a more usable catalog of complete albums. (It does some other stuff too, as I was experimenting with mp3 tags.) `copy_albums.py` will take that more usable catalog and create a new directory structure based on albums, rather than artists.

Constants for directory locations are currently defined at the package level (in `__init__.py`), which isn't ideal, but was pragmatic at the time. 

## Setup

I used `pipenv`, so there's a `Pipfile` with all the needed requirements (there aren't many).