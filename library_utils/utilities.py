from functools import reduce

def predicate_split(p, iterable):
    def split(lists, item):
        if p(item):
            lists[0].append(item)
        else:
            lists[1].append(item)
        return lists

    return reduce(split, iterable, (list(), list()))
