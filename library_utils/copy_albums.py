from shutil import copyfile
from os.path import join
from os import mkdir
from song_lister import build_library
from library_utils import destination_root

def write_album(album):
    print("Writing %s" % album.album_name)
    directory = join(destination_root, album.album_name)
    mkdir(directory)
    for song in album.songs:
        new_path = join(directory, song.song_name)
        copyfile(song.song_path, new_path)


def copy_albums():
    albums = [album for title, album in build_library().items()]
    for a in albums:
        write_album(a)

if __name__ == "__main__":
    copy_albums()