from collections import defaultdict
from os import listdir
from mutagen.mp3 import MP3
import mutagen
from models import Artist
from models import Album
from library_utils import library_root


def build_library():
    artists = [Artist(a) for a in listdir(library_root)]

    albums = defaultdict(Album)
    #tags = defaultdict(list)
    for artist in artists:
        for a_album in artist.albums:
            albums[a_album.album_name].add_album(a_album)
            # for song in a_album.songs:
                # song_tags = song.mp3_file.tags
                # for t in song_tags:
                #     if not 'PRIV:Google/OriginalClientId' in t and not 'PRIV:Google/StoreId' in t:
                #         tags[t].append(song)

    return albums




if __name__ == "__main__":
    build_library()
    print("done")