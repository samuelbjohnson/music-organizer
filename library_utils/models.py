from os import listdir
from os import path
from os import remove as remove_file
from library_utils import library_root
from mutagen import File

from utilities import predicate_split

class Song:
    def __init__(self, artist, album_name = '', song_name = ''):
        self.artist = artist
        self.album_name = album_name
        self.song_name = song_name
        self.song_path = path.join(library_root, artist, album_name, song_name)
        if not path.isfile(self.song_path):
            raise Exception
        if "(1).mp3" in song_name:
            remove_file(self.song_path)
        self.mp3_file = File(self.song_path)


class ArtistAlbum:
    def __init__(self, artist, album_name):
        self.artist = artist
        self.album_name = album_name
        album_path = path.join(library_root, artist, album_name)
        (songs, other) = predicate_split(lambda p: path.isfile(path.join(album_path, p)), listdir(album_path))
        self.songs = [Song(artist, album_name, song) for song in songs]
        if other:
            raise Exception


class Album:
    def __init__(self):
        self.album_name = ''
        self.artists = list()
        self.songs = list()

    def add_album(self, album):
        if not self.album_name:
            self.album_name = album.album_name
        if self.album_name == album.album_name:
            self.artists.append(album.artist)
            self.songs.extend(album.songs)
        else:
            raise Exception('Album %s matched album %s' % (self.album_name, album.album_name))


class Artist:
    def __init__(self, name):
        self.name = name
        (songs, albums) = predicate_split(lambda p: path.isfile(path.join(library_root, name, p)),
                                          listdir(path.join(library_root, name)))
        self.albums = [ArtistAlbum(name, album_name) for album_name in albums]
        self.songs = [Song(name, song_name=song_name) for song_name in songs]
